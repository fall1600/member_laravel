<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $visible = ['id', 'name', 'email', 'created_at', 'updated_at'];
}
