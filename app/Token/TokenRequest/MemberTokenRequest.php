<?php

namespace App\Token\TokenRequest;

use App\Member;

class MemberTokenRequest extends AbstractDecoratorTokenRequest
{
    const TOKEN_TYPE = 'member';
    const TTL = 3600;

    /** @var Member  */
    protected $member;

    public function __construct(Member $member)
    {
        $this->member = $member;
    }

    public function getId()
    {
        return $this->member->id;
    }

    public function getData()
    {
        return array();
    }

    public function getType()
    {
        return self::TOKEN_TYPE;
    }

    public function getTtl()
    {
        return self::TTL;
    }
}
