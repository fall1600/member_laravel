<?php

namespace App\Token\TokenRequest;

interface TokenRequestInterface
{
    public function getId();
    public function getType();
    public function getData();
    public function getTtl();
}
