<?php

namespace App\Token;

use App\Token\TokenRequest\TokenRequestInterface;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\ValidationData;

class ShaTokenService extends AbstractTokenService
{
    /** @var string $issuer */
    protected $issuer;

    /** @var string $issuer */
    protected $secret;

    public function __construct()
    {
        if (is_null($this->issuer)) {
            $this->injectIssuer(getenv("TOKEN_ISSUER"));
        }

        if (is_null($this->secret)) {
            $this->injectSecret(getenv("TOKEN_SECRET"));
        }
    }

    public function injectIssuer($issuer)
    {
        $this->issuer = $issuer;
    }

    public function injectSecret($secret)
    {
        $this->secret = $secret;
    }

    /**
     * @param TokenRequestInterface $tokenRequest
     * @return string
     */
    public function sign(TokenRequestInterface $tokenRequest): string
    {
        $signer = new Sha256();
        return (string) $this->applyData($this->issuer, $tokenRequest)
            ->sign($signer, $this->secret)
            ->getToken();
    }

    /**
     * @param $jwtToken
     * @return Token|null
     */
    public function parse($jwtToken)
    {
        $result = $this->verify($jwtToken);
        if ($result === false) {
            return null;
        }
        return new Token($result);
    }

    protected function verify($jwtToken)
    {
        try {
            $token = (new Parser())->parse($jwtToken);
            if (!$token->verify(new Sha256(), $this->secret)) {
                return false;
            }

            $current = $this->getCurrentTime();
            $validationData = new ValidationData();
            $validationData->setIssuer($this->issuer);
            $validationData->setCurrentTime($current);
            if (!$token->validate($validationData)) {
                return false;
            }
            return json_decode(json_encode($token->getClaims()), true);
        } catch (\Exception $exception) {
            return false;
        }
    }
}
