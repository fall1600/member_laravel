<?php

namespace App\Token;

use App\Token\TokenRequest\TokenRequestInterface;

interface TokenServiceInterface
{
    /**
     * @param TokenRequestInterface $tokenRequest
     * @return string
     */
    public function sign(TokenRequestInterface $tokenRequest): string;

    /**
     * @param $jwtToken
     * @return Token|null
     */
    public function parse($jwtToken);
}
