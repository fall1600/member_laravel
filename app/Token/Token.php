<?php

namespace App\Token;

class Token
{
    protected $claims;

    public function __construct($claims)
    {
        $this->claims = $claims;
    }

    public function getId()
    {
        return $this->claims['id']??null;
    }

    public function getType()
    {
        return $this->claims['type']??null;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->claims['data']??array();
    }

    /**
     * 取得全部欄位
     * @return array
     */
    public function getClaims()
    {
        return $this->claims;
    }

    /**
     * @param $type
     * @return boolean
     */
    public function isType($type)
    {
        return $type === $this->getType();
    }
}
