<?php

namespace App\Service;

use App\Http\Requests\Member\MemberEnable;
use App\Http\Requests\Member\MemberLogin;
use App\Http\Requests\Member\MemberRegister;
use App\Http\Requests\Member\MemberForgetPassword;
use App\Http\Requests\Member\MemberResetPassword;
use App\Mail\ForgetPasswordMail;
use App\Mail\MemberRegisterMail;
use App\Member;
use App\Token\ShaTokenService;
use App\Token\TokenRequest\MemberTokenRequest;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Foundation\Testing\WithFaker;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MemberService
{
    use WithFaker;

    const PASSWORD_ALGORITHM = PASSWORD_ARGON2I;

    const PASSWORD_TIME_COST = 12;

    const TTL_NORMAL = 3600;

    const TTL_FORGET_PASSWORD = 600;

    protected $tokenService;

    protected $mailer;

    public function __construct(ShaTokenService $tokenService, Mailer $mailer)
    {
        $this->tokenService = $tokenService;
        $this->mailer = $mailer;
    }

    public function register(MemberRegister $parameters)
    {
        $member = $this->create(
            $parameters['name'], $parameters['email'], $parameters['plain_password']
        );
        $this->mailer->send(new MemberRegisterMail($member));
        return $member;
    }

    public function login(MemberLogin $parameters)
    {
        $member = $this->fetchMemberByEmail($parameters['email']);
        if (!$member) {
            throw new NotFoundHttpException();
        }

        if (!$member->enabled) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED);
        }

        if (!$this->verifyPassword($member, $parameters['password'])) {
            throw new HttpException(Response::HTTP_BAD_REQUEST);
        }

        return $this->tokenService->sign(new MemberTokenRequest($member));
   }

    public function enable(MemberEnable $parameters)
    {
        $member = $this->fetchMemberByEmail($parameters['email']);
        if (!$member) {
            throw new NotFoundHttpException();
        }

        if ($member->confirm_token != $parameters['confirm_token']) {
            throw new HttpException(Response::HTTP_BAD_REQUEST);
        }

        if (strtotime($member->confirm_token_expired_at) < time()) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }

        $member->enabled = true;
        $member->confirm_token = null;
        $member->confirm_token_expired_at = null;
        $member->save();
        return $member;
    }

    public function forgetPassword(MemberForgetPassword $parameters)
    {
        $member = $this->fetchMemberByEmail($parameters['email']);
        if (!$member) {
            throw new NotFoundHttpException();
        }

        if (!$member->enabled) {
            throw new HttpException(Response::HTTP_UNAUTHORIZED);
        }

        if ((strtotime($member->confirm_token_expired_at)-time()) > self::TTL_FORGET_PASSWORD/2) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }

        $this->resetConfirmToken($member, self::TTL_FORGET_PASSWORD);
        $member->save();

        $this->mailer->send(new ForgetPasswordMail($member));
    }

    public function resetPassword(MemberResetPassword $parameters)
    {
        $member = $this->fetchMemberByEmail($parameters['email']);
        if (!$member) {
            throw new NotFoundHttpException();
        }

        if ($member->confirm_token !== $parameters['confirm_token']) {
            throw new HttpException(Response::HTTP_BAD_REQUEST);
        }

        if (strtotime($member->confirm_token_expired_at) < time()) {
            throw new HttpException(Response::HTTP_FORBIDDEN);
        }

        $member->password = $this->hashPassword($parameters['plain_password']);
        $member->enabled = true;
        $member->confirm_token = null;
        $member->confirm_token_expired_at = null;
        $member->save();
        return $member;
    }

    /**
     * @param string $email
     * @return \Illuminate\Database\Eloquent\Model|null|object|static
     */
    public function fetchMemberByEmail(string $email)
    {
        return Member::query()
            ->where('email', '=', $email)
            ->first();
    }

    protected function create(string $name, string $email, string $plainPassword)
    {
        $member = new Member();
        $member->name = $name;
        $member->email = $email;
        $member->password = $this->hashPassword($plainPassword);
        $this->resetConfirmToken($member);
        $member->save();
        return $member;
    }

    protected function hashPassword(string $plainPassword)
    {
        $options = array(
            'time_cost' => self::PASSWORD_TIME_COST
        );
        return password_hash($plainPassword, self::PASSWORD_ALGORITHM, $options);
    }

    protected function verifyPassword(Member $member, string $plainPassword)
    {
        return password_verify($plainPassword, $member->password);
    }

    protected function resetConfirmToken(Member $member, int $ttl = self::TTL_NORMAL)
    {
        $member->confirm_token = $this->faker("zh_TW")->sha256;
        $member->confirm_token_expired_at = date('Y-m-d H:i:s', time() + $ttl);
    }
}
