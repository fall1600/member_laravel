<?php

namespace App\Mail;

use App\Member;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgetPasswordMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    protected $member;

    protected $frontendHost;

    protected $frontendRoute;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Member $member)
    {
        $this->member = $member;
        $this->frontendHost = env("FRONTEND_HOST");
        $this->frontendRoute = env("FRONTEND_ROUTE_FORGET_PASSWORD");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $encodeEmail = urlencode($this->member->email);
        return $this
            ->from(getenv("EMAIL_FROM"))
            ->to($this->member->email)
            ->view('emails.forget-password')
            ->with([
                'name' => $this->member->name,
                'magic_link' => "{$this->frontendHost}/{$this->frontendRoute}?e={$encodeEmail}&t={$this->member->confirm_token}"
            ]);
    }
}
