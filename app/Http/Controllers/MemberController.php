<?php

namespace App\Http\Controllers;

use App\Http\Requests\Member\MemberEnable;
use App\Http\Requests\Member\MemberLogin;
use App\Http\Requests\Member\MemberRegister;
use App\Http\Requests\Member\MemberForgetPassword;
use App\Http\Requests\Member\MemberResetPassword;
use App\Service\MemberService;
use Symfony\Component\HttpKernel\Exception\HttpException;

class MemberController extends Controller
{
    protected $memberService;

    public function __construct(MemberService $memberService)
    {
        $this->memberService = $memberService;
    }

    public function login(MemberLogin $request)
    {
        try {
            $result = $this->memberService->login($request);
            return response()->json($result);
        } catch (HttpException $exception) {
            return response()->json($exception->getMessage(), $exception->getStatusCode());
        }
    }

    public function register(MemberRegister $request)
    {
        $result = $this->memberService->register($request);
        return response()->json($result);
    }

    public function enable(MemberEnable $request)
    {
        try {
            $result = $this->memberService->enable($request);
            return response()->json($result);
        } catch (HttpException $exception) {
            return response()->json($exception->getMessage(), $exception->getStatusCode());
        }
    }

    public function forgetPassword(MemberForgetPassword $request)
    {
        try {
            $this->memberService->forgetPassword($request);
            return response()->json();
        } catch (HttpException $exception) {
            return response()->json($exception->getMessage(), $exception->getStatusCode());
        }
    }

    public function resetPassword(MemberResetPassword $request)
    {
        try {
            $result = $this->memberService->resetPassword($request);
            return response()->json($result);
        } catch (HttpException $exception) {
            return response()->json($exception->getMessage(), $exception->getStatusCode());
        }
    }
}
