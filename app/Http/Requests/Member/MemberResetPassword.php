<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class MemberResetPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => "required|email",
            'confirm_token' => "required",
            'plain_password' => "required",
        ];
    }

    public function messages()
    {
        return [
            'email.email' => "信箱格式錯誤",
            'email.required' => "信箱不得為空",
            'confirm_token.required' => "驗證碼不得為空",
            'plain_password' => "密碼不得為空",
        ];
    }
}
