<?php

namespace App\Http\Requests\Member;

use Illuminate\Foundation\Http\FormRequest;

class MemberRegister extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => "required|max:255",
            'email' => "required|unique:members|email",
            'plain_password' => "required"
        ];
    }

    public function messages()
    {
        return [
            'name.required' => "名字不得為空",
            'email.email' => "信箱格式錯誤",
            'email.required' => "信箱不得為空",
            'email.unique' => "信箱已使用",
            'plain_password' => "密碼不得為空",
        ];
    }
}
