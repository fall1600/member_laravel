<h3>You forget password</h3>

Hi {{ $name }}! You forget your password, right?

To reset your password, go to: <a href="{{ $magic_link }}">Click Me!</a>.

Thanks!
