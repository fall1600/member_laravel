<!doctype html>
<html>
<head>
</head>
<body style="background: black; color: white">
<h3>You did it! You registered!</h3>

Hi {{ $name }}! You're successfully registered.

To enable your account, go to: <a href="{{ $magic_link }}">Click Me!</a>.

Thanks!

</body>
</html>
