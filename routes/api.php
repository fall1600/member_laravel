<?php

use Barryvdh\Cors\HandleCors;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware(['cors'])->group(function () {
    Route::put('member/login', "MemberController@login");
    Route::post('members', "MemberController@register");
    Route::post('member/enable', "MemberController@enable");
    Route::put('member/forget-password', "MemberController@forgetPassword");
    Route::post('member/reset-password', "MemberController@resetPassword");
});
